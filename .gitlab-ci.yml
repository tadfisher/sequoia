before_script:
  - if [ -d $CARGO_TARGET_DIR ]; then find $CARGO_TARGET_DIR | wc --lines; du -sh $CARGO_TARGET_DIR; fi
  - if [ -d $CARGO_HOME ]; then find $CARGO_HOME | wc --lines; du -sh $CARGO_HOME; fi
  - rustc --version
  - cargo --version
  - clang --version

after_script:
  - find $CARGO_TARGET_DIR -type f -atime +7 -delete
  - du -sh $CARGO_TARGET_DIR
  - du -sh $CARGO_HOME

buster:
  tags:
    - linux
  stage: build
  image: registry.gitlab.com/sequoia-pgp/build-docker-image/buster:latest

  script:
    - make codespell CODESPELL_FLAGS=--summary
    - SEQUOIA_CTEST_VALGRIND=/usr/bin/valgrind cargo test -p buffered-reader -p sequoia-openpgp -p sequoia-sqv -p sequoia-openpgp-ffi

  variables:
    CARGO_TARGET_DIR: $CI_PROJECT_DIR/../target.$CI_CONCURRENT_ID.buster

bullseye:
  tags:
    - linux
  stage: build
  image: registry.gitlab.com/sequoia-pgp/build-docker-image/bullseye:latest

  script:
    - make test # XXX: We cannot currently use valgrind, https://github.com/rust-lang/rust/issues/68979
    - make
    - if ! git diff --quiet Cargo.lock ; then echo "Cargo.lock changed.  Please add the change to the corresponding commit." ; false ; fi
    - make -C tool update-usage
    - if ! git diff --quiet tool ; then echo "Please run 'make -C tool update-usage' and commit the result." ; false ; fi
    - make -C sqv update-usage
    - if ! git diff --quiet sqv ; then echo "Please run 'make -C sqv update-usage' and commit the result." ; false ; fi
    - if ! git diff --quiet ; then echo "The build changed the source.  Please investigate." ; git diff ; fi

  variables:
    CARGO_TARGET_DIR: $CI_PROJECT_DIR/../target.$CI_CONCURRENT_ID.bullseye
    RUSTFLAGS: -D warnings -A unused-parens

rust-stable:
  tags:
    - linux
  stage: build
  image: rust:1-slim-buster

  before_script:
    - apt update -y -qq
    - apt install --no-install-recommends -y -qq -o=Dpkg::Use-Pty=0 clang libclang-dev llvm pkg-config nettle-dev
    - if [ -d $CARGO_TARGET_DIR ]; then find $CARGO_TARGET_DIR | wc --lines; du -sh $CARGO_TARGET_DIR; fi
    - if [ -d $CARGO_HOME ]; then find $CARGO_HOME | wc --lines; du -sh $CARGO_HOME; fi
    - rustc --version
    - cargo --version
    - clang --version

  script:
    - cargo test -p buffered-reader -p sequoia-openpgp -p sequoia-sqv

  variables:
    CARGO_TARGET_DIR: $CI_PROJECT_DIR/../target.$CI_CONCURRENT_ID.rust-stable
    RUSTFLAGS: -D warnings -A unused-parens

windows-gnu:
  tags:
  - win
  - win2019
  stage: build
  # This job takes ~40 minutes to run, let's only execute it manually or for
  # scheduled builds, otherwise this will stall MRs often not related to Windows
  only:
  - /windows/i # refs containing 'windows' keyword
  - tags
  - web
  - scheduled
  before_script:
  - $env:USERNAME
  - $env:USERPROFILE
  # Install Rust 1.40
  - $env:CARGO_HOME = "$env:ProgramFiles\cargo"
  - $env:RUSTUP_HOME = "$env:ProgramFiles\rustup"
  - Invoke-WebRequest https://win.rustup.rs -OutFile rustup-init.exe
  - .\rustup-init.exe -y --default-toolchain 1.40.0 --default-host x86_64-pc-windows-gnu --profile minimal
  - $env:Path += ";$env:ProgramFiles\cargo\bin"
  - rustup --version
  - rustc --version --verbose
  # Install chocolatey
  - Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
  # Install MSYS2/mingw-w64
  - choco install msys2 -y
  - $env:Path += ";C:\tools\msys64"
  - $env:MSYSTEM =  "MINGW64"
  - $env:MSYS = "winsymlinks:nativestrict"
  - $env:MSYS2_PATH_TYPE = "inherit" # Inherit PATH from Windows
  - $env:CHERE_INVOKING = "1" # Inherit working directory
  - $env:CFLAGS = "" # Disregard C warnings due to MinGW being funky
  script: C:/tools/msys64/usr/bin/bash.exe -l -c "bash -x ./.gitlab-ci/test-msys2.sh"
  after_script: []

windows-msvc:
  tags:
  - win
  - win2019
  stage: build
  # This job takes ~40 minutes to run, let's only execute it manually or for
  # scheduled builds, otherwise this will stall MRs often not related to Windows
  only:
  - /windows/i # refs containing 'windows' keyword
  - tags
  - web
  - scheduled
  before_script:
  - $env:USERNAME
  - $env:USERPROFILE
  # Install VS 2019 VC++ Build Tools (Required for Rust on Windows)
  - Get-Date
  - Invoke-WebRequest https://aka.ms/vs/16/release/vs_buildtools.exe -OutFile vs_buildtools.exe
  # Wait until the VC++ Build Tools installation is completed.
  # See https://github.com/MicrosoftDocs/visualstudio-docs/issues/970
  - >
    $exitCode = Start-Process
    -FilePath vs_buildtools.exe
    -ArgumentList "--norestart", "--quiet", "--nocache", "--add", "Microsoft.VisualStudio.Workload.VCTools", "--includeRecommended", "--wait"
    -Wait
    -PassThru
  # Install Rust 1.40
  - Get-Date
  - $env:CARGO_HOME = "$env:ProgramFiles\cargo"
  - $env:RUSTUP_HOME = "$env:ProgramFiles\rustup"
  - Invoke-WebRequest https://win.rustup.rs -OutFile rustup-init.exe
  - .\rustup-init.exe -y --default-toolchain 1.40.0 --default-host x86_64-pc-windows-msvc --profile minimal
  - $env:Path += ";$env:ProgramFiles\cargo\bin"
  - Get-Date
  - rustup --version
  - rustc --version --verbose
  script:
    - cd openpgp
    # https://github.com/rust-lang/cargo/issues/5015
    - cargo test --no-default-features --features compression,crypto-cng
  after_script: []
  variables:
    CFLAGS: '' # Silence some C warnings when compiling with MSVC

variables:
  DEBIAN_FRONTEND: noninteractive
  CARGO_HOME: $CI_PROJECT_DIR/../cargo
  CARGO_FLAGS: --color always
  CARGO_INCREMENTAL: 0
  RUST_BACKTRACE: full
  RUSTFLAGS: -D warnings
  CFLAGS: -Werror
  QUICKCHECK_GENERATOR_SIZE: 500 # https://github.com/BurntSushi/quickcheck/pull/240
